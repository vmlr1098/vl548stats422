import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
import seaborn as sns
from plotnine import *
from plotnine import (

    ggplot,
    ggtitle,
    aes,
    after_stat,
    geom_density,
    geom_histogram,
    geom_vline,
    geom_rect,
    labs,
    annotate,
    theme_tufte,
    facet_wrap,
    facet_grid,
    coord_flip,
    scale_y_continuous,
    scale_y_sqrt,
    scale_y_log10,
    scale_fill_manual,
    theme_bw,
    theme_xkcd,
)



# 2A  ####################################################################################################
url = 'https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2023/2023-05-30/centenarians.csv'
df = pd.read_csv(url, index_col=0)
print(df.head(5))


# create a histogram of the age variable
(
    ggplot(df, aes(x="age"))
    + ggtitle("Histogram of Age")
    + geom_histogram()
)

# a density plot of the age variable
(
    ggplot(df, aes(x="age"))
    + ggtitle("Density Plot of Age")
    + geom_density()
)

# create a boxplot of the age variable
            # min(df['age'])
            # max(df['age'])
            # df["age"].value_counts()

            # age_rounddown=np.floor(df['age'])
            # age_df = pd.value_counts(age_rounddown).reset_index()
            # age_df.columns = ['age', 'count']
            # age_df

            # (
            #      ggplot(age_df, aes("age", "count"))
            #      + geom_boxplot()
            # )
plt.boxplot(df['age'])
plt.show()

# What is more important here is “the story”. 
# If you had to use one of these graphics to talk 
# about age in this dataset to your 
# coworkers/supervisors/thesis advisor, 
# which one would you prefer to use and why?
'''
I would use the histogram because it shows the general distribution of age.
We can observe the peaks of the age distribution, and note the distribution 
is right skewed. This suggests an outlier at age 122.
'''



# 2B  ####################################################################################################
# Please create one new visualization of age and add a second variable. 
(
    ggplot(df, aes(x="age", color="gender", fill="gender"))
    + geom_density(alpha=0.1)
    + ggtitle("Density Plot of Age, by Gender")
)

# So for example, you could add gender and compare boxplots of age. 
(
     ggplot(df, aes("gender", "age", fill="gender"))
     + geom_boxplot()
     + ggtitle("Boxplots of Age, by Gender")
)

# Whatever you choose to do, keep the storytelling aspect in mind. 
# Do some visualizations work better than others? 
# Do they make talking about the relationship between age and second variable easier?
'''
I think visualizations that include a second variable depict a more in-depth picture of the dataset.
Both the density and boxplot that include 'gender' are better plots as we can get start to form
a story about age distribution. For example, we can see that females tend to live longer than males.
'''



# 3B  ####################################################################################################

df2 = pd.read_csv("data_for_416.csv", header=[0], skipinitialspace=True, index_col=0)
df2.head(5)
list(df2.columns)

(
    ggplot(df2, aes(x="YearOnly", color="School_Level", fill="School_Level"))
    + geom_density(aes(y=after_stat("count")), alpha=0.1)
    + ggtitle("Density Plots of School Shootings\n in CA (1970-2022), by School Level")

)


(
     ggplot(df2, aes("longitude", "latitude", color="School_Level"))
     + geom_point(alpha=0.1)
     + ggtitle("Lat/Lon Scatterplot of School Shootings\n in CA (1970-2022), by School Level")
)


(
    ggplot(df2, aes(x="School_Level", fill="School_Level"))
    + ggtitle("Histogram of School Shootings\n in CA (1970-2022), per School Level")
    + geom_histogram()
)



# 3C  ##################################################################################################
'''
Based on #2A and #2B, I realized how self-explanatory the general distribution of a 
particular variable is, when using histograms and layered density plots. This led
me to create the plots I did, and I added color because it's easier (in my opinion) to
differentiate between graphs.

Although we didn't discuss scatterplots in the hw, I wanted to give it a try, but
using latitude and longitude points, as well as school levels, to obtain the distribution
of school shootings per level of education, across the state of CA.
'''



# 3D  ##################################################################################################
'''
The CHDS website (where I downloaded the first .csv file from) illustrates that there
have been 2,069 school shootings in America from 1970 - 2022. Of those shootings, 
there have been 684 fatalities, and 1,937 injuries. I decided to focus solely on the 
state of California, which dropped our number of observations to 497.

I was most interested in the distribution of school shootings per school level, and per
location (lat/lon) in CA, to see if there was some visual telling patterns.

My first plot (Density Plots of School Shootings in CA (1970-2022), by School Level)
made it very clear that there was a peak for elementary school shootings in the late 80's/
early 90's. Although elementary schools had the highest peak, high school shootings
seemed to be very prevalent throughout. In conjunction with my third plot 
(Histogram of School Shootings in CA (1970-2022), per School Level), I was able to analyze
the plots a bit further. Despite having the largest peak, high schools had more school
shootings than elementary schools.

Further, my second plot (Lat/Lon Scatterplot of School Shootings in CA (1970-2022), 
by School Level) visualizes the locations of the school shootings. What I found interesting 
is that the majority of the high schools targeted were on the border of CA, while
more elementary and junior high were in the middle.
'''